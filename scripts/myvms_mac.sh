#!/bin/bash

# This is a port to MacOS by Espen Wobbes, espewo@stud.ntnu.no

# Updated to support macOS Sonoma by Markus Kristiansen, markkris@stud.ntnu.no

# This script assumes you have microsoft remote desktop installed
# Netcat is a standard in MacOS, so you won't have to install it by yourself
# usage: sh myvms_mac.sh [mgr|cl1|dc1|srv1|mgra|cl1a|dc1a|srv1a]

# The microsoft remote desktop does not allow the password to be pasted inn automatically
# The script copy's the correct password to your clipboard instead.
# To login to your vm, use keyboard shortcut cmd + v and hit enter
# You should get logged in now.

# floating ip's and the Admin-user-password for each host (retrieve from SkyHiGh):
mgr_ip=''
mgr_pw=''
cl1_ip=''
cl1_pw=''
dc1_ip=''
dc1_pw=''
srv1_ip=''
srv1_pw=''
# domain administrator password (you set this yourself when creating the domain):
dc1a_pw=''

logintype="$1"

logmein () {
    echo "Trying to login to $logintype with IP $ip"
    if nc -z -w2 "$ip" 3389; then
        echo "Login $logintype"
        open -u "rdp://full%20address=s%3A$ip&username=s%3A$user"
        echo $pw | tr -d '\n' | pbcopy
    else
        echo "Not able to reach port 3389 on host $ip"
    fi
}

case "$logintype" in
    "mgr")
        ip=$mgr_ip
        pw="$mgr_pw"
        user=Admin
        logmein
        exit 0;;
    "cl1")
        ip=$cl1_ip
        pw="$cl1_pw"
        user=Admin
        logmein
        exit 0;;
    "dc1")
        ip=$dc1_ip
        pw="$dc1_pw"
        user=Admin
        logmein
        exit 0;;
    "srv1")
        ip=$srv1_ip
        pw="$srv1_pw"
        user=Admin
        logmein
        exit 0;;
    "mgra")
        ip=$mgr_ip
        pw="$dc1a_pw"
        user="SEC%5CAdministrator"
        logmein
        exit 0;;
    "cl1a")
        ip=$cl1_ip
        pw="$dc1a_pw"
        user="SEC%5CAdministrator"
        logmein
        exit 0;;
    "dc1a")
        ip=$dc1_ip
        pw="$dc1a_pw"
        user="SEC%5CAdministrator"
        logmein
        exit 0;;
    "srv1a")
        ip=$srv1_ip
        pw="$dc1a_pw"
        user="SEC%5CAdministrator"
        logmein
        exit 0;;
    *)
        echo "Please tell me which login you would like."
        exit 1;;
esac
